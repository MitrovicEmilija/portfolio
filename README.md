# Getting started

## Zagon React.js "foto-atelje" projekta lokalno
Ko prenesete in razpakirate projekt sa GitLab-a, v terminalu editora ali pa v command prompt-u v direktorijumu projekta zaženemo s pomočjo: npm start.
Odprite http://localhost:3000 da boste ga lahko pogledali v brskalniku.

## Zagon Jakarta projekta
Za zagon Jakarta projekta potrebno je prenesti in namestiti aplikacijski strežnik. Spodaj bom priložila detaljna navodila kako zagnati celotno aplikacijo:
https://univerzamb-my.sharepoint.com/:w:/g/personal/emilija_mitrovic_student_um_si/ETGiyrBzXbxHsvDDoP4lTaYB5yJVXlhj3_3f4wtTe3qrcg?e=JvebRl

